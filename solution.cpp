#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP(x , y) 			make_pair(x ,y)
#define 	MPP(x , y , z) 	    MP(x, MP(y,z))
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<long long ,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define II ({int a; scanf("%d", &a); a;})
#define LL ({ll a; scanf("%lld", &a); a;})

typedef long long ll;

using namespace std;
vector<vector<ll> > vec;
ll n ,m , xx , yy ,tmp[102];
//ll st ,fi;
//ll miny()
//{
//    ll sum =0  ,qq=0 ,maxcur =-inf ,fmax=-inf;
//    Rep(i , n-xx+1)
//    {
//        sum =0;
//        For(j ,i , i+xx)
//        {
//            sum+=tmp[j] , maxcur =max(maxcur, sum);
//        }
//
//    }
//    return maxcur;
//}
//ll F()
//{
//    ll maxx = -inf;
//    Rep(i , m)
//    {
//        Set(tmp , 0);
//        For(j , i , i+yy)
//        {
//            if(j==m)
//                break;
//            Rep(k , n)
//                tmp[k]+=vec[k][j];
//            ll cur =miny();
//            maxx=max(maxx, cur);
//        }
//    }
//    return maxx;
//}
ll F()
{
    ll maxx=-inf ,sum;
    For(i , xx , n+1)
    {
        For(j , yy , m+1){
             sum =vec[i][j] - vec[i-xx][j] - vec[i][j-yy] +vec[i-xx][j-yy]  ,maxx =max(maxx , sum);
        }
    }
    return maxx;
}

int main(){
    ll t;      // Test;
    cin>>t;
    while(t--)
    {
        cin>>n >> m;
        vec.assign(n+5 ,vector<ll>(m+5 , 0));
        For(i , 1, n+1)
        {
            For(j , 1, m+1){ll x ; cin>>x; x+=vec[i-1][j]+vec[i][j-1]-vec[i-1][j-1] , vec[i][j]=x;}
        }
        ll q;
        cin>>q;
        Rep(p ,q){
            cin>>xx>>yy;
            cout<<F()<<" ";
        }
        cout<<endl;
    }
}